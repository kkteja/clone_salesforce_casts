class RenameBuyoutToPublishInSeries < ActiveRecord::Migration
  def change
  	rename_column :series, :buyout, :publish
  end
end
