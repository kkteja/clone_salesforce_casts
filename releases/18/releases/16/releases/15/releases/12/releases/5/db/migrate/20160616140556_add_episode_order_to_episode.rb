class AddEpisodeOrderToEpisode < ActiveRecord::Migration
  def change
    add_column :episodes, :episode_order, :integer
  end
end
