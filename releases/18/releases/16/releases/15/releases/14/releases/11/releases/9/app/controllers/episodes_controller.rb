class EpisodesController < ApplicationController


  before_action :authenticate_user!
  before_action :current_user_admin?, except: [:show]
  before_action :find_episode, only: [:show, :edit, :update, :destroy]	
  before_action :current_user_has_subscription?, only: [:show]
  # before_action :check_if_episode_belongs_to_series?, only: [:show]
	
  def new
  	@episode = Episode.new
  end

  def create
    @series = Series.find(params[:series_id])
    @series.update_attribute('no_of_lessons',@series.no_of_lessons+1)
    @episode = @series.episodes.new(episode_params)

      if @episode.save
      redirect_to :back, notice: "Episode created successfully !!"
    else
      render :new
    end
  end

  def show
    @series = @episode.series
    @episodes = @series.episodes.order(episode_order: :asc)
  end

  def edit
  end

  def update
    if @episode.update(episode_params)

      flash[:notice] = "Episode updated successfully !!"
      # redirect_to action: "show", id: @episode.id
      # redirect_to series_episode_path(@episode.series_id,@episode)
      # redirect_to series_path(@episode.series_id)
      
      redirect_to [@episode.series, @episode]
      
    else

      flash[:notice] = "Something went wrong, please retry !!"
      render 'edit'
    end
  end

  def destroy
    @episode.destroy

      flash[:notice] = "Series deleted successfully !!"

    redirect_to :back, notice: "Episode deleted successfully !!"
  end


  private
  def find_episode
	 @episode = Episode.find(params[:id])

  end

  def episode_params
	 params.require(:episode).permit(:episode_title, :episode_description,:episode_video,:series_id,:tags,
									:episode_type,:episode_duration,:episode_order )	
  end

  def current_user_admin?
    if current_user
      if !current_user.admin
        flash[:notice] = "You are not authorised to this page"
        
        redirect_to root_url

      end  
    end  
    
  end

  def current_user_has_subscription?
    if not @episode.series.orders.where(user_id: current_user.id).length == 1 and not current_user.admin
      flash[:notice] = "You need to subscribe to get access to the course"

      redirect_to series_path(@episode.series)
      
    end
  end

  # def check_if_episode_belongs_to_series?

  #   if Episode.where(series_id: @episode.series_id, id: params[:id]).blank?  
    
  #     flash[:notice] = "Sorry, episode deosnt belong to this series."
  #     redirect_to series_path(@episode.series) 
  #   end 
  # end

end
