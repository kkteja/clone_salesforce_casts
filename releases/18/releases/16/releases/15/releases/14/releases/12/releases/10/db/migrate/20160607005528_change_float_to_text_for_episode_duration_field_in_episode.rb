class ChangeFloatToTextForEpisodeDurationFieldInEpisode < ActiveRecord::Migration
	def up
	    change_column :episodes, :episode_duration, :string
	end

	def down
	    change_column :episodes, :episode_duration, :float
	end
end
