class Order < ActiveRecord::Base
  belongs_to :series
	belongs_to :user
	has_many :transactions, class_name: "OrderTransaction"	
  
  attr_accessor :card_number, :card_verification, :buy_or_rent
  #4032034846427006


  before_create :validate_card
  
  def purchase

  	# byebug
    response = GATEWAY.purchase(price_in_cents, credit_card, purchase_options)
    transactions.create!(action: "purchase", amount: price_in_cents, response: response)
    # cart.update_attribute(:purchased_at, Time.now) if response.success?
    response.success?
  end
  
  def price_in_cents
    if buy_or_rent == "rent"

      (series.rent_price*100).round
    elsif buy_or_rent == "buy"

      (series.buyout_price*100).round
    else
    end      
  end

  private
  def purchase_options
    {
      :ip => ip_address,

      # :ip => "49.205.123.7",

      billing_address: {
        name: "Ryan Bates",
        address1: "123 Main St.",
        city: "New York",
        state: "NY",
        country: "US",
        zip: "10001"
      }
    }
  end
  
  def validate_card
    unless credit_card.valid?
      credit_card.errors.full_messages.each do |message|
        errors.add :base, message
      end
    end
  end
  
  def credit_card
    
    @credit_card ||= ActiveMerchant::Billing::CreditCard.new(
      type: card_type,
      number: card_number,
      verification_value: card_verification,
      month: card_expires_on.month,
      year: card_expires_on.year,
      first_name: first_name,
      last_name: last_name
    )
  end
end
