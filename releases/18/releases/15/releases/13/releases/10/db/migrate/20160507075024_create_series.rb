class CreateSeries < ActiveRecord::Migration
  def change
    create_table :series do |t|
      t.string :title
      t.text :description
      t.integer :no_of_lessons
      t.text :difficulty
      t.text :trailer
      t.boolean :buyout
      t.decimal :rent_price
      t.decimal :buyout_price

      t.timestamps null: false
    end
    add_index :series, :title


    execute "SELECT setval('series_id_seq', 1000000)"

  end

end
