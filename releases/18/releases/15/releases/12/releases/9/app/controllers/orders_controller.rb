class OrdersController < ApplicationController


before_action :authenticate_user!
before_action :rent_or_buy?
before_action :find_order, only: [:show]
before_action :has_permissions?, only: [:show]


  def rent
    @order.buy_or_rent = :rent 
    render :new
  end

  def buy
    @order.buy_or_rent = :buy 
    render :new
  end

  def create
  	# @series = Series.find(params[:series_id])

  	@order = current_user.orders.build(order_params)
  	@order.ip_address = request.remote_ip

  	@order.series_id = params[:series_id]

  	if @order.save
	  	if @order.purchase 
	  		flash[:notice] = "Series is successfully purchased"
	  		# redirect_to [@order.series,@order]
        redirect_to subscriptions_series_index_path
	  	else
	  		
	  		redirect_to series_path(@order.series), notice: "Something went wrong can your purchase please re-try"
	  	end
  	else
        redirect_to series_path(@order.series), notice: "Something went wrong can your purchase please re-try"
  	end  	
  	
  end

  def show
   
  end

  private

  def find_order
    @order = Order.find(params[:id])
   

  end
  def order_params
  	params.require(:order).permit(:first_name, :last_name,:card_type,:card_expires_on,:card_number, :card_verification, :series_id,:buy_or_rent)
  end
  def rent_or_buy?
    @series = Series.find(params[:series_id])
    @order = Order.new
  end
  def has_permissions?

    if Order.where(user_id: current_user, id: params[:id], series_id: params[:series_id]).blank? 
      flash[:notice] = "Sorry, you dont have access to this."
      redirect_to series_path(@order.series)
    else
       
    end
  end

end
 