# -*- encoding: utf-8 -*-
# stub: mina-sidekiq 0.4.0 ruby lib

Gem::Specification.new do |s|
  s.name = "mina-sidekiq"
  s.version = "0.4.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Joerg Thalheim"]
  s.date = "2016-05-16"
  s.description = "Adds tasks to aid in the deployment of Sidekiq"
  s.email = ["joerg@higgsboson.tk"]
  s.homepage = "http://github.com/Mic92/mina-sidekiq"
  s.licenses = ["MIT"]
  s.post_install_message = "Starting with 0.2.0, you have to add:\n\n    require \"mina_sidekiq/tasks\"\n\nin your deploy.rb to load the library\n"
  s.rubygems_version = "2.2.2"
  s.summary = "Tasks to deploy Sidekiq with mina."

  s.installed_by_version = "2.2.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<mina>, [">= 0"])
    else
      s.add_dependency(%q<mina>, [">= 0"])
    end
  else
    s.add_dependency(%q<mina>, [">= 0"])
  end
end
