class SeriesController < ApplicationController


before_action :authenticate_user!, except: [:index, :show, :browse, :search, :callback]
before_action :current_user_admin?, except: [:index, :show, :browse, :search, :subscriptions, :callback ]
before_action :find_series, only: [:show, :show_admin_view, :edit, :update, :destroy, :publish, :unpublish]

  def index
    @series = Series.order(created_at: :desc).where(publish: true).limit(8)
  end

  def new
  	@series = Series.new
  end

  def create
  	@series = Series.new(series_params)
  	 if @series.save
  	 	redirect_to @series
  	 else
  	 	render :new
  	 end
  end

  def show
    @episodes = Episode.where(series_id: params[:id]).order(episode_order: :asc)
  end

  def show_admin_view
    @episodes = Episode.where(series_id: params[:id])
  end

  def edit
  end

  def update
    # AppMailer.welcome_email(current_user).deliver_now  

    if @series.update(series_params)

      flash[:notice] = "Series updated successfully !!"
      redirect_to @series
    else

      flash[:notice] = "Something went wrong, please retry !!"
      render 'edit'
    end
  end

  def destroy
    @series.destroy

      flash[:notice] = "Series deleted successfully !!"

    redirect_to :back
  end

  def publish
    @series.publish = true
    if @series.save

      flash[:notice] = "Series published successfully !!"
      redirect_to :back
    else

      flash[:notice] = "Something went wrong, please retry !!"
      redirect_to root_url
    end
  end

  def unpublish
    @series.publish = false
    if @series.save

      flash[:notice] = "Series un-published successfully !!"
      redirect_to :back
    else

      flash[:notice] = "Something went wrong, please retry !!"
      redirect_to root_url
    end
  end

  def search
    if params[:search].present?
      @series = Series.where("lower(title) LIKE ?","%#{params[:search].downcase}%")
    else
      @series = Series.order(created_at: :desc).where(publish: true)
    end

    render 'browse'
  end

  def browse
    
    if params[:filter_difficulty].present? 
      @series = Series.where("lower(difficulty) LIKE ?","%#{params[:filter_difficulty].downcase}%")
      # @filter_difficulty = params[:filter_difficulty]

    else
      @series = Series.order(created_at: :desc).where(publish: true)
    end
    


    respond_to do |f|
      f.html
      f.js
    end
  end

  def dashboard
    @series = Series.all()
      
  end

  def subscriptions
    @orders = Order.where(user_id: current_user.id).where.not(series_id: nil).order(created_at: :desc)
  end

  def callback
    @series = Series.order(created_at: :desc).where(publish: true).limit(2)
    
  end

  def users 
    @users = User.all
  end


  private
	def find_series
		@series = Series.find(params[:id])
	end
	def series_params
		params.require(:series).permit(:title, :description,:no_of_lessons,:difficulty,:trailer,
									   :rent_price,:buyout_price,:series_banner )
	end

  def current_user_admin?
    if user_signed_in?
      if !current_user.admin
        flash[:notice] = "You are not authorised to this page"
        
        redirect_to root_url

      end  
      

    end 
    
  end
end
